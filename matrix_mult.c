#include <stdio.h>
#include <malloc.h>
#include <time.h>

#include <mpi.h>

#define ENABLE_LOG 0

#define N1 4
#define N2 4
#define N3 4

void matrix_print(const double *a, size_t n1, size_t n2);

void matrix_mul(const double *a, const double *b, double *res, size_t n1, size_t n2, size_t n3);

void matrix_trans_self(double *a, size_t n1, size_t n2);

void make_default_a(double *a, size_t n1, size_t n2);

void make_default_b(double *b, size_t n2, size_t n3);

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    int dims[2] = {0, 0}, periods[2] = {0, 0}, col[2] = {1, 0}, row[2] = {0, 1}, coords[2], reorder = 1;
    int size, rank, sizey, sizex, ranky, rankx;
    int prevy, prevx, nexty, nextx;

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Dims_create(size, 2, dims);

    sizey = dims[0];
    sizex = dims[1];

    MPI_Comm comm2d;
    MPI_Comm comm_row;
    MPI_Comm comm_column;

    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &comm2d);
    MPI_Cart_sub(comm2d, row, &comm_row);
    MPI_Cart_sub(comm2d, col, &comm_column);

    MPI_Comm_rank(comm2d, &rank);
    MPI_Cart_get(comm2d, 2, dims, periods, coords);
    ranky = coords[0];
    rankx = coords[1];
    MPI_Cart_shift(comm2d, 0, 1, &prevy, &nexty);
    MPI_Cart_shift(comm2d, 1, 1, &prevx, &nextx);

    double *a_whole, *b_whole, *c_whole, *a_part, *b_part, *c_part, *c_temp;
    int m1, m2;

    m1 = N2 / sizey;
    m2 = N3 / sizex;

    a_part = (double *) malloc(m1 * N2 * sizeof(double));
    b_part = (double *) malloc(N2 * m2 * sizeof(double));
    c_part = (double *) malloc(m1 * m2 * sizeof(double));

    if (rank == 0) {
        a_whole = (double *) malloc(N1 * N2 * sizeof(double));
        b_whole = (double *) malloc(N2 * N3 * sizeof(double));
        c_whole = (double *) malloc(N1 * N3 * sizeof(double));

        make_default_a(a_whole, N1, N2);
        make_default_b(b_whole, N2, N3);

        if (ENABLE_LOG) {
            printf("A whole: \n");
            matrix_print(a_whole, N1, N2);
            printf("\nB whole: \n");
            matrix_print(b_whole, N2, N3);
        }

        matrix_trans_self(b_whole, N2, N3);
    }

    if (rankx == 0) {
        c_temp = (double *) malloc(m1 * N3 * sizeof(double));
        MPI_Scatter(a_whole, m1 * N2, MPI_DOUBLE, a_part, m1 * N2, MPI_DOUBLE, 0, comm_column);
    }
    if (ranky == 0)
        MPI_Scatter(b_whole, m2 * N2, MPI_DOUBLE, b_part, m2 * N2, MPI_DOUBLE, 0, comm_row);

    MPI_Bcast(a_part, m1 * N2, MPI_DOUBLE, 0, comm_row);
    MPI_Bcast(b_part, m2 * N2, MPI_DOUBLE, 0, comm_column);

    matrix_trans_self(b_part, m2, N2);
    matrix_mul(a_part, b_part, c_part, m1, N2, m2);

    for (int i = 0; i < m1; i++)
        MPI_Gather(c_part + i * m2, m2, MPI_DOUBLE, c_temp + i * m2 * sizex, m2, MPI_DOUBLE, 0, comm_row);

    if (rankx == 0)
        MPI_Gather(c_temp, m1 * N3, MPI_DOUBLE, c_whole, m1 * N3, MPI_DOUBLE, 0, comm_column);

    if (ENABLE_LOG) {
        for (int i = 0; i < size; i++) {
            MPI_Barrier(MPI_COMM_WORLD);
            if (rank == i) {
                printf("\n********************\n");

                printf("I'm %d (%d, %d).\n\nA part: \n", rank, rankx, ranky);
                matrix_print(a_part, m1, N2);

                printf("\nB part: \n");
                matrix_print(b_part, N2, m2);

                printf("\nC part: \n");
                matrix_print(c_part, m1, m2);

                if (rankx == 0) {
                    printf("\nC temp: \n");
                    matrix_print(c_temp, m1, N3);
                }

            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    if (rank == 0) {
        printf("\nDone!\n");

        if (ENABLE_LOG) {
            printf("\nC: \n");
            matrix_print(c_whole, N1, N3);
        }

        free(a_whole);
        free(b_whole);
        free(c_whole);
    }
    if (rankx == 0)
        free(c_temp);

    free(a_part);
    free(b_part);
    free(c_part);

    MPI_Finalize();

    return 0;
}

void matrix_mul(const double *a, const double *b, double *res, size_t n1, size_t n2, size_t n3) {

    /* a is n1 x n2 matrix
     * b is n2 x n3 matrix
     * c is n1 x n3 matrix
     */

    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n3; j++) {
            res[i * n3 + j] = 0.0;
            for (int r = 0; r < n2; r++)
                res[i * n3 + j] += a[i * n2 + r] * b[r * n3 + j];
        }
    }
}

void matrix_print(const double *a, size_t n1, size_t n2) {
    for (int i = 0; i < n1; i++) {
        for (int j = 0; j < n2; j++) {
            printf("%5.2f ", a[i * n2 + j]);
        }
        printf("\n");
    }
}

void make_default_a(double *a, size_t n1, size_t n2) {
    for (int i = 0; i < n1; i++)
        for (int j = 0; j < n2; j++)
            a[i * n2 + j] = (double) i;
}

void make_default_b(double *b, size_t n2, size_t n3) {
    for (int i = 0; i < n2; i++)
        for (int j = 0; j < n3; j++)
            b[i * n3 + j] = (i == j) ? 2.0 : 0.0;
}

void matrix_trans_self(double *a, size_t n1, size_t n2) {

    /* In-place matrix transposition is a complex problem which does not have one simple solution.
     * I have tried implementing simple cycling algorithms, but have seen no improvement in overall performance,
     * while memory usage was still far more than O(1). In the end I have decided to use simple algorithm
     * that uses additional O(n1*n2) memory. This is okay for this test task, but should be avoided in favour
     * of more complex solutions in "real" code.
     */

    double *t = (double *)malloc(n1 * n2 * sizeof(double));

    for (int i = 0; i < n1 * n2; i++)
        t[i] = a[i];

    for (int i = 0; i < n1; i++)
        for (int j = i; j < n2; j++)
            a[j * n1 + i] = t[i * n2 + j];

    free(t);
}
